Truecaller Test App
=========

****

Design
-----------

**Package structure**

* auth: Contains authentication/authorisation related classes, currently only HTTP Basic auth
* config: Dropwizard config
* data: Data Access related classes
* data.entities: Hibernate/JPA entity classes
* resources: JAX-RS resource classes
* resources.views: JSON representation of user and user views
* util: Various stuff, but currently only j.u.Optional mapping for Jersey

**Schema**

The database schema is quite simple. There are two tables:
* users
* user_views

The users table currently only has a password column, along with an auto incrementing primary key.

The user_views table has a timestamp column, along with a user FK which is the user that was viewed, and a viewed_by column, which is also a user FK with the difference that the user corresponds to the user that did the viewing.

The user_views table is an append only table, which basically means that nothing is ever deleted. The reason for that is that it is a simpler design (at least initially) and since we're anyway constraining how many user views can be fetched at a time, I didn't see the point and this could be changed down the road if this turns out to be too non-performant. 

For the same reasons as outlined above, I have decided not to implement any kind of batch jobs. It is my opinion that if it does turn out to be a performance problem, one could then implement a batch task.

Compromises that had to be made were more related to Hibernate than to the db schema itself. I wanted to have a join table that mapped a viewed user with the viewer, along with a timestamp. Hibernate doesn't really support that use case (or it doesn't support it that well), so I had to do some contortions with the mapping of the View id (converting it to an ``@Embeddable``) for it to work.

The way I see it it's the way that makes the most sense, at least in a purely RDBMS system. It is easier to make various tradeoffs if one borrows storage concepts from NoSQL systems such as Cassandra. One could for example store each view in a single column as a map of <date, user_id>, which would make sense if the querying pattern always requires a fetching of all views when a user is queried for. This would be more efficient in terms of latency and storage space, with the tradeoff that it's more difficult to ensure data integrity.

Installation
--------------

```sh
mvn clean package -DskipTests # There are integration tests that will break since we don't have a schema yet
java -jar target/truecaller-test-1.0-SNAPSHOT.jar db migrate truecaller.yaml # Create schema
mvn clean package # Now we can build with tests running
java -jar target/truecaller-test-1.0-SNAPSHOT.jar server truecaller.yaml # Run application
```
