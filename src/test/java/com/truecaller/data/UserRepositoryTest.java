package com.truecaller.data;

import com.truecaller.data.entities.User;
import com.truecaller.data.entities.View;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.*;

public class UserRepositoryTest {
    private final SessionFactory sessionFactory;
    private final UserRepository userRepository;
    private final UserDAO userDAO;
    private final ViewDAO viewDAO;

    public UserRepositoryTest() {
        AnnotationConfiguration config = new AnnotationConfiguration();
        config.setProperty("hibernate.connection.url","jdbc:h2:~/truecaller-test-db");
        config.setProperty("hibernate.connection.username","sa");
        config.setProperty("hibernate.connection.password","sa");
        config.setProperty("hibernate.connection.driver_class","org.h2.Driver");
        config.setProperty("hibernate.current_session_context_class", "thread");
        config.setProperty("hibernate.show_sql", "false");
        config.addAnnotatedClass(User.class);
        config.addAnnotatedClass(View.class);

        sessionFactory = config.buildSessionFactory();
        userDAO = new UserDAO(sessionFactory);
        viewDAO = new ViewDAO(sessionFactory);
        userRepository = new UserRepository(viewDAO, userDAO);
    }

    @Before
    public void setUp() {
        getSession().beginTransaction();
    }

    @Test
    public void testCreateUser() {
        com.truecaller.resources.views.User user = userRepository.saveUser("password");
        getSession().getTransaction().commit();

        getSession().beginTransaction();
        assertTrue(userRepository.findUserWithViews(user.getId()).isPresent());
        getSession().getTransaction().commit();
    }

    @Test
    public void testFindUserWithSameId() {
        com.truecaller.resources.views.User user = userRepository.saveUser("password");
        Optional<com.truecaller.resources.views.User> u = userRepository.findUser(user.getId(), userDAO.find(user.getId()).get(), new Date());
        getSession().getTransaction().commit();

        assertEquals(0, u.get().getViews().size());
    }

    @Test
    public void testFindUserWithDifferentId() {
        com.truecaller.resources.views.User user = userRepository.saveUser("password");
        com.truecaller.resources.views.User anotherUser = userRepository.saveUser("password");
        userRepository.findUser(user.getId(), userDAO.find(anotherUser.getId()).get(), new Date());
        getSession().getTransaction().commit();


        getSession().beginTransaction();
        assertEquals(1, userRepository.findUserWithViews(user.getId()).get().getViews().size());
    }

    @Test
    public void testFindUserWithMax10Days() {
        com.truecaller.resources.views.User user = userRepository.saveUser("password");
        com.truecaller.resources.views.User anotherUser = userRepository.saveUser("password");

        for (int i = 0; i <= 10; i++) {
            userRepository.findUser(user.getId(), userDAO.find(anotherUser.getId()).get(), new Date(Instant.now().minus(i + 5, ChronoUnit.DAYS).toEpochMilli()));
        }

        getSession().getTransaction().commit();

        getSession().beginTransaction();
        assertEquals(5, userRepository.findUserWithViews(user.getId()).get().getViews().size());
    }

    @Test
    public void testFindUserWithMax10Entries() {
        com.truecaller.resources.views.User user = userRepository.saveUser("password");
        com.truecaller.resources.views.User anotherUser = userRepository.saveUser("password");

        for (int i = 0; i < 20; i++) {
            userRepository.findUser(user.getId(), userDAO.find(anotherUser.getId()).get(), new Date());
        }

        getSession().getTransaction().commit();



        getSession().beginTransaction();
        assertEquals(10, userRepository.findUserWithViews(user.getId()).get().getViews().size());
    }

    private Session getSession() {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (SessionException se) {
            session = sessionFactory.openSession();
        }

        return session;
    }
}