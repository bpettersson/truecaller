package com.truecaller.resources;

import com.truecaller.data.UserRepository;
import com.truecaller.data.entities.User;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import java.util.Date;
import java.util.Optional;

/**
 * @author blake
 */
@Path("/users")
public class UserResource {
    private final UserRepository repository;

    public UserResource(UserRepository repository) {
        this.repository = repository;
    }

    @POST
    @UnitOfWork
    @Produces("application/json")
    public com.truecaller.resources.views.User createUser(@FormParam("password") String password) {
        return repository.saveUser(password);
    }

    @GET
    @UnitOfWork
    @Path("{id}")
    @Produces("application/json")
    public Optional<com.truecaller.resources.views.User> getUser(@Auth User user, @PathParam("id") long userId) {
        return repository.findUser(userId, user, new Date());
    }

    @GET
    @UnitOfWork
    @Path("{id}/views")
    @Produces("application/json")
    public Optional<com.truecaller.resources.views.User> getUserViews(@Auth User user, @PathParam("id") long userId) {
        return repository.findUserWithViews(userId);
    }
}
