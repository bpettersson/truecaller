package com.truecaller.resources.views;

import java.util.ArrayList;
import java.util.List;

/**
 * @author blake
 */
public class User {
    private long id;
    private List<View> views;

    public User() {
    }

    public User(long id) {
        this(id, new ArrayList<>());
    }

    public User(long id, List<View> views) {
        this.id = id;
        this.views = views;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<View> getViews() {
        return views;
    }

    public void setViews(List<View> views) {
        this.views = views;
    }
}
