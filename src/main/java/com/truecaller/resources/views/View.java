package com.truecaller.resources.views;

import java.util.Date;

/**
 * Created by blake on 13/09/14.
 */
public class View {
    public long userId;
    public Date timestamp;

    public View() {
    }

    public View(long userId, Date timestamp) {
        this.userId = userId;
        this.timestamp = timestamp;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
