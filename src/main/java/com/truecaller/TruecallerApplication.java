package com.truecaller;

import com.truecaller.auth.SimpleAuthenticator;
import com.truecaller.config.TruecallerConfiguration;
import com.truecaller.data.UserDAO;
import com.truecaller.data.UserRepository;
import com.truecaller.data.ViewDAO;
import com.truecaller.data.entities.User;
import com.truecaller.data.entities.View;
import com.truecaller.resources.UserResource;
import com.truecaller.util.JulOptionalResourceMethodDispatchAdapter;
import io.dropwizard.Application;
import io.dropwizard.auth.basic.BasicAuthProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * @author blake
 */
public class TruecallerApplication extends Application<TruecallerConfiguration> {
    private final HibernateBundle<TruecallerConfiguration> hibernate = new HibernateBundle<TruecallerConfiguration>(User.class, View.class) {
        @Override
        public DataSourceFactory getDataSourceFactory(TruecallerConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };

    @Override
    public void initialize(Bootstrap<TruecallerConfiguration> bootstrap) {
        bootstrap.addBundle(hibernate);
        bootstrap.addBundle(new MigrationsBundle<TruecallerConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(TruecallerConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
    }

    @Override
    public void run(TruecallerConfiguration config, Environment environment) throws Exception {
        UserDAO userDAO = new UserDAO(hibernate.getSessionFactory());
        ViewDAO viewDAO = new ViewDAO(hibernate.getSessionFactory());

        //CacheBuilder<Object, Object> cacheBuilder = CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.MINUTES);
        //CachingAuthenticator<BasicCredentials, User> authenticator = new CachingAuthenticator<>(environment.metrics(), new SimpleAuthenticator(userDAO), cacheBuilder);

        // Register support for returning java.util.Optional in resources
        environment.jersey().register(JulOptionalResourceMethodDispatchAdapter.class);
        environment.jersey().register(new BasicAuthProvider<>(new SimpleAuthenticator(userDAO), "my realm"));
        environment.jersey().register(new UserResource(new UserRepository(viewDAO, userDAO)));
    }


    public static void main(String... args) throws Exception {
        new TruecallerApplication().run(args);
    }
}
