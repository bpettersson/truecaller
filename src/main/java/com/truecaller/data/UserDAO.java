package com.truecaller.data;

import com.truecaller.data.entities.User;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.Optional;

/**
 * @author blake
 */
public class UserDAO extends AbstractDAO<User> {
    public UserDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Optional<User> find(long id) {
        return Optional.ofNullable(get(id));
    }

    public User save(User user) {
        return persist(user);
    }
}
