package com.truecaller.data;

import com.truecaller.resources.views.User;
import com.truecaller.resources.views.View;

import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author blake
 */
public class UserRepository {
    private final ViewDAO viewDAO;
    private final UserDAO userDAO;

    public UserRepository(ViewDAO viewDAO, UserDAO userDAO) {
        this.viewDAO = viewDAO;
        this.userDAO = userDAO;
    }

    public User saveUser(String password) {
        com.truecaller.data.entities.User user = new com.truecaller.data.entities.User();
        user.setPassword(password);
        return new User(userDAO.save(user).getId());
    }

    public Optional<User> findUser(long id, com.truecaller.data.entities.User loggedInUser, Date date) {
        Optional<com.truecaller.data.entities.User> userOptional = userDAO.find(id);

        userOptional.filter(u -> u.getId() != loggedInUser.getId()).map(u -> {
            com.truecaller.data.entities.View view = new com.truecaller.data.entities.View();
            view.setUser(u);
            view.setViewedBy(loggedInUser);
            view.setTimestamp(date);

            u.getViews().add(view);

            viewDAO.save(view);
            userDAO.save(u);
            return u;
        });

        return userOptional.map(u -> new com.truecaller.resources.views.User(u.getId()));
    }

    public Optional<User> findUserWithViews(long id) {
        Optional<com.truecaller.data.entities.User> userOptional = userDAO.find(id);
        return userOptional.map(u -> new com.truecaller.resources.views.User(u.getId(), viewDAO.findByUser(u, 10, 10).stream().map(v -> new View(v.getViewedBy().getId(), v.getTimestamp())).collect(Collectors.toList())));
    }
}
