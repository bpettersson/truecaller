package com.truecaller.data.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author blake
 *
 */
@Embeddable
public class ViewId implements Serializable {
    private User user;
    private User viewedBy;
    private Date timestamp;

    @ManyToOne
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column
    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @ManyToOne
    public User getViewedBy() {
        return viewedBy;
    }

    public void setViewedBy(User viewedBy) {
        this.viewedBy = viewedBy;
    }
}
