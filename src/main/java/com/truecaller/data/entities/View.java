package com.truecaller.data.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * @author blake
 */
@Entity
@Table(name = "user_views")
@AssociationOverrides({
    @AssociationOverride(name = "pk.user", joinColumns = @JoinColumn(name = "user_id")),
    @AssociationOverride(name = "pk.viewedBy", joinColumns = @JoinColumn(name = "viewed_by_id"))
})
public class View {
    private ViewId pk = new ViewId();

    @EmbeddedId
    public ViewId getPk() {
        return pk;
    }

    public void setPk(ViewId pk) {
        this.pk = pk;
    }

    @Transient
    public Date getTimestamp() {
        return pk.getTimestamp();
    }

    public void setTimestamp(Date timestamp) {
        pk.setTimestamp(timestamp);
    }

    @Transient
    public User getUser() {
        return pk.getUser();
    }

    public void setUser(User user) {
        pk.setUser(user);
    }

    @Transient
    public User getViewedBy() {
        return pk.getViewedBy();
    }

    public void setViewedBy(User viewedBy) {
        pk.setViewedBy(viewedBy);
    }
}
