package com.truecaller.data;

import com.truecaller.data.entities.User;
import com.truecaller.data.entities.View;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

/**
 * @author blake
 */
public class ViewDAO extends AbstractDAO<View> {

    public ViewDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public View save(View view) {
        return persist(view);
    }

    public List<View> findByUser(User user, int maxDays, int maxResults) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime nowMinusMaxDays = now.minus(maxDays, ChronoUnit.DAYS).with(ChronoField.HOUR_OF_DAY, 0);

        return currentSession()
                .createQuery("from View v where v.pk.user = :user and v.pk.timestamp between :nowMinusMaxDays and :now order by v.pk.timestamp desc")
                .setMaxResults(maxResults)
                .setParameter("user", user)
                .setParameter("now", new Date(now.toInstant(ZoneOffset.UTC).toEpochMilli()))
                .setParameter("nowMinusMaxDays", new Date(nowMinusMaxDays.toInstant(ZoneOffset.UTC).toEpochMilli()))
                .list();
    }
}
