package com.truecaller.auth;

import com.google.common.base.Optional;
import com.truecaller.data.UserDAO;
import com.truecaller.data.entities.User;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;

/**
 *
 * @author blake
 *
 */
public class SimpleAuthenticator implements Authenticator<BasicCredentials, User> {
    private final UserDAO dao;

    public SimpleAuthenticator(UserDAO dao) {
        this.dao = dao;
    }

    @Override
    public Optional<User> authenticate(BasicCredentials credentials) throws AuthenticationException {
        //Convert a java.util.Optional to a com.google.common.base.Optional
        return Optional.fromNullable(dao.find(Long.valueOf(credentials.getUsername())).filter(u -> u.getPassword().equals(credentials.getPassword())).orElse(null));
    }
}